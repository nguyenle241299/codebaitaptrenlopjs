// render dssv 
function layThongTinTuForm() {
    var maSv = document.getElementById("txtMaSV").value;
    var tenSv = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var matkhau = document.getElementById("txtPass").value;
    var diemToan = document.getElementById("txtDiemToan").value *  1;
    var diemLy = document.getElementById("txtDiemLy").value *  1;
    var diemHoa = document.getElementById("txtDiemHoa").value *  1;

    var sv = new sinhVien(maSv, tenSv, email, matkhau, diemToan, diemLy, diemHoa);
     return sv;
}

function showThongTinLenForm(sv) {
    document.getElementById("txtMaSV").value = sv.ma
    document.getElementById("txtTenSV").value = sv.ten
    document.getElementById("txtEmail").value = sv.email
    document.getElementById("txtPass").value = sv.matkhau
    document.getElementById("txtDiemToan").value = sv.diemToan
    document.getElementById("txtDiemLy").value = sv.diemLy
    document.getElementById("txtDiemHoa").value = sv.diemHoa
} 

function renderDssv(list) {
    var contentHTML = "";

    for( var i = 0; i < list.length; i++) {
        var currentSv = list[i];
        var contentTr = `
            <tr>
                <td>${currentSv.ma}</td>
                <td>${currentSv.ten}</td>
                <td>${currentSv.email}</td>
                <td>${currentSv.tinhDTB()}</td>
                <td>
                    <button onclick = "xoaSv('${currentSv.ma}')" class="btn btn-primary">Xóa</button>
                    <button onclick = "suaSv('${currentSv.ma}')" class="btn btn-danger">Sửa</button>
                </td>
            </tr>
        `
        contentHTML += contentTr;
    }

    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function resetForm(){
    document.getElementById("formQLSV").reset();
}