var dssv = [];

const DSSV = "DSSV";

var dataJson = localStorage.getItem(DSSV);

if(dataJson) {
    var dataRaw = JSON.parse(dataJson);
    // var result = [];
    // for ( var index = 0; index < dataRaw.length; index++) {
    //     var currentData = dataRaw[index];
    //     var sv = new sinhVien(
    //         currentData.ma,
    //         currentData.ten,
    //         currentData.email,
    //         currentData.matkhau,
    //         currentData.diemToan,
    //         currentData.diemLy,
    //         currentData.diemHoa,
    //     );
    //     result.push(sv);     
    // }
    // dssv = result;
    dssv = dataRaw.map(function(item){
        return new sinhVien(
            item.ma,
            item.ten,
            item.email,
            item.matkhau,
            item.diemToan,
            item.diemLy,
            item.diemHoa,
        );
    })

    renderDssv(dssv);
}

function saveLocalStorage() {
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
}

function themSv() {
    var newSv = layThongTinTuForm();
    // Tạo object lấy thông tin từ form
    dssv.push(newSv);

    // Lưu xuống local
    saveLocalStorage();
    renderDssv(dssv);  
    resetForm(); 
}

function xoaSv(idSv) {
    var index = dssv.findIndex(function(sv){
        return sv.ma == idSv;
    });
    if(index == -1) return;

    dssv.splice(index, 1);
    renderDssv(dssv);
    saveLocalStorage();
}

function suaSv(idSv) {
    var index = dssv.findIndex(function(sv){
        return sv.ma = idSv;
    });
    if(index == -1) return;
    var sv = dssv[index];
    showThongTinLenForm(sv);

    document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv(){
    var svEdit = layThongTinTuForm();

    var index = dssv.findIndex(function(sv){
        return sv.ma = svEdit.ma;
    });

    if(index == -1) return;

    dssv[index] = svEdit;

    renderDssv(dssv);
    saveLocalStorage();
    resetForm();
    document.getElementById("txtMaSV").disabled = false;

}